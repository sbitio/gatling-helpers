#!/usr/bin/python3

import re

f = open("nav.txt", "r")

print("|Step|Action|Status|URL|Referer|")
print("|---|---|---|---|---|")
p= re.compile("(.*) \{(.*)\} \{.*\} (.*)")
step = 0
for line in f.readlines():
    match = re.match(p, line)

    status = match.groups()[0].split(" ")[8]
    referer = match.groups()[1].split("|")[-2]

    g3 = match.groups()[2].strip('"').split(" ")
    action = g3[0]
    url = g3[1]

    print("|%d|%s|%s|%s|%s|" % (step, action, status, url, referer))
    step = step + 1

f.close()
