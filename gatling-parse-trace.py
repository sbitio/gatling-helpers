#!/usr/bin/python3

import re, mmap

marker_start = rb'>>>>>>>>>>>>>>>>>>>>>>>>>>'
marker_end = rb'<<<<<<<<<<<<<<<<<<<<<<<<<'

with open('results/log', 'r+') as f:
  data = mmap.mmap(f.fileno(), 0)
  pages = re.findall(rb'%s(.*?)%s' % (marker_start, marker_end), data, re.MULTILINE|re.DOTALL)
  for count, page in enumerate(pages):
      with open('results/page%s.html' % (count), 'wb') as fp:
          fp.write(page)
      fp.close()
f.close()
