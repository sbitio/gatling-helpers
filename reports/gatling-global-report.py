#!/usr/bin/python3

import argparse
import glob
import json
import logging
import os
import re
import shutil

parser = argparse.ArgumentParser(
    prog='gatling-global-report.py',
    description='Generates a summary of Gatling reports stored in Jenkins builds.')

parser.add_argument('--src', required=True, help="path to Jenkins job builds folder")
parser.add_argument('--dst', required=True, help="path to store summary files")
parser.add_argument('--jenkins-url', required=True, help="Url of jenkins installation")
parser.add_argument('-s', '--simulations', required=True, nargs="+", help="name of simulations to process")
parser.add_argument('-d', '--debug', required=False, action='store_true', help="Show debug information")

args = parser.parse_args()

BASEPATH=args.src
SIMULATIONS=args.simulations
DESTDIR=args.dst
JENKINS_URL=args.jenkins_url

if args.debug:
    logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger()

def natural_sort(l):
    convert = lambda text: int(text) if text.isdigit() else text.lower()
    alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)]
    return sorted(l, key=alphanum_key)

for SIMULATION in SIMULATIONS:
    logger.debug("Simulation: %s" % SIMULATION)
    GLOB = BASEPATH + "/builds/*/simulations/" + SIMULATION + "-*"
    PATTERN = re.compile(".*/builds/(\d+)/simulations/" + SIMULATION + "-(\d{4})(\d{2})(\d{2}).*")

    files = natural_sort([f for f in glob.glob(GLOB) if '/builds/last' not in f])
    for f in files:
        logger.debug("Build dir: %s" % f)
        entry = {}

        match = re.match(PATTERN, f)
        build_id = match.groups()[0]
        date = "/".join(match.groups()[1:4])
        logger.debug("build id: %s" % build_id)

        storage_dir = "%s/%s/%s" % (DESTDIR, SIMULATION, build_id)
        logger.debug("Build storage dir: %s" % storage_dir)
        if os.path.exists(storage_dir):
            logger.debug("Storage dir already exists. Skipping job")
            continue

        entry['simulation'] = SIMULATION
        entry['date'] = date

        jobUrl = BASEPATH.replace("/var/lib/jenkins", JENKINS_URL).replace("jobs", "job") + "/" + build_id
        entry['jobUrl'] = jobUrl
        entry['reportUrl'] = jobUrl + "/Gatling_20Reports/" + f.split('/')[11] + "/index.html"

        with open(f + "/js/global_stats.json") as global_stats_file:
            global_stats = json.load(global_stats_file)

            # Get the load (i.e. 500 users / 100s ramp)
            desc = os.popen("head -n 1 " + os.path.realpath(f + '/simulation.log') + "| awk -F\\\t '{print $5}'").read().strip()
            logger.debug("Description: %s" % desc)
            # There're several formats for the description:
            #  - legacy 1: dev ; U users; Rs ramp
            #  - legacy 2: SimulationName__u-80_r-40_paramN-valueN
            #  - current: json including {"users": "U", "ramp", "Rs"}
            if desc.startswith('{'):
                desc = json.loads(desc)
                users = desc['users']
                ramp = desc['ramp'][:-1]
            elif 'users;' in desc:
                desc = desc.split(';')[-2:]
                users = desc[0].strip().split(' ')[0]
                ramp = desc[1].strip().split(' ')[0][:-1]
            elif '__u-' in desc:
                desc = desc.split('_')[2:4]
                users = desc[0].split('-')[1]
                ramp = desc[0].split('-')[1]
            else:
                logger.error("Build %s: Invalid description format: «%s». Build skipped." % (build_id, desc))
                continue
            logger.debug("Users: %s; Ramp: %s" % (users, ramp))

            # Get the start and end date
            start = os.popen("head -n 1 " + os.path.realpath(f + '/simulation.log') + "| awk -F\\\t '{print $4}'").read().strip()
            end = os.popen("tail -n 1 " + os.path.realpath(f + '/simulation.log') + "| awk -F\\\t '{print $4}'").read().strip()
            if end:
                duration = "%d" % ((int(end) - int(start)) / 100)
            else:
                duration = '-'

            # Get the total of users that reach the last request
            try:
                stats = json.load(open(os.path.realpath(f + '/js/stats.json')))
                last = [ item for item in stats['contents'].items() if item[1]["path"] == "LAST" ][0][1]
                success = last['stats']['numberOfRequests']['ok']
            except:
                success = '0'

            entry['users'] = users
            entry['ramp'] = ramp
            entry['duration'] = duration
            entry['success'] = success
            entry['group1'] = "%d (%d%%)" % (global_stats['group1']['count'], global_stats['group1']['percentage'])
            entry['group2'] = "%d (%d%%)" % (global_stats['group2']['count'], global_stats['group2']['percentage'])
            entry['group3'] = "%d (%d%%)" % (global_stats['group3']['count'], global_stats['group3']['percentage'])

        shutil.copytree(f, storage_dir + '/html')
        with open("%s/summary.json" % (storage_dir), 'w') as fp:
          json.dump(entry, fp, indent = 4)
