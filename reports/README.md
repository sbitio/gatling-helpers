## reports/gatling-global-report.py

Parsea todos los reportes de simulaciones Gatling almacenados en un job
jenkins y los almacena en otra ubicación junto con un resumen del resultado
en formato json:

Ejemplo del json generado:

```
# cat /var/gatling/reports/dev/pagessimulation/303/summary.json
{
    "simulation": "pagessimulation",
    "date": "2024/01/29",
    "jobUrl": "https://jenkins.example.com/job/loadtest/job/gatling-devel/303",
    "reportUrl": "https://jenkins.example.com/job/loadtest/job/gatling-devel/303/Gatling_20Reports/pagessimulation-20240129161318655/index.html",
    "users": "80",
    "ramp": "40s",
    "duration": "402 seconds",
    "success": "80 (100.0%)",
    "group1": "1835 (92%)",
    "group2": "80 (4%)",
    "group3": "85 (4%)"
```

La key `success` indica el número de usuarios que han llegado hasta un step gatling de nombre "LAST".
Para que funcione debes añadir un step como:

```
.exec(http("LAST")
  .get("")
  .check(
    status.is(200)
  )
)
```

## reports/gatling-global-report-to-html.py

Convierte los resúmenes generados por el comando anterior en informes html.

## Ejemplo de uso combinado de ambos scripts

```
JENKINS_DIR=/var/lib/jenkins/jobs/WHATEVER/jobs/PRO-gatling
STORAGE_DIR=/var/gatling/reports/PRO
REPORTS_DIR=/var/www/html/gatling-summary-PRO
SIMULATIONS="simulation1 simulation2"

# Generate summaries
/opt/sbitio-gatling-helpers/reports/gatling-global-report.py \
  --src $JENKINS_DIR \
  --dst $STORAGE_DIR \
  -s $SIMULATIONS

# Generate html tables from summaries
/opt/sbitio-gatling-helpers/reports/gatling-global-report-to-html.py \
  --src $STORAGE_DIR \
  --dst $REPORTS_DIR \
  -s $SIMULATIONS
```
