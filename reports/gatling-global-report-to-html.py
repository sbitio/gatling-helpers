#!/usr/bin/python3

import argparse
import glob
import json
import logging
import re

parser = argparse.ArgumentParser(
    prog='gatling-global-report-to-html.py',
    description='Generates a html with a summary of Gatling reports post-processed by gatling-global-report.py.')

parser.add_argument('--src', required=True, help="path to storage dir of gatling-global-report.py")
parser.add_argument('--dst', required=True, help="path to store html summary")
parser.add_argument('-s', '--simulations', required=True, nargs="+", help="name of simulations to process")
parser.add_argument('-d', '--debug', required=False, action='store_true', help="Show debug information")

args = parser.parse_args()

SRC=args.src
DST=args.dst
SIMULATIONS=args.simulations

if args.debug:
    logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger()

def natural_sort(l):
    convert = lambda text: int(text) if text.isdigit() else text.lower()
    alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)]
    return sorted(l, key=alphanum_key)

for SIMULATION in SIMULATIONS:
    logger.debug("Simulation: %s" % SIMULATION)

    # Classify entries by users/ramp
    entries = {}
    GLOB = SRC + "/" + SIMULATION + "/*/summary.json"
    files = natural_sort([f for f in glob.glob(GLOB)])
    for f in reversed(files):
        with open(f) as f2:
            entry = json.load(f2)
            group = "%(users)s-%(ramp)s" % entry
            if group not in entries:
                entries[group] = {
                    "title": "%(users)s users / %(ramp)ss ramp" % entry,
                    "entries": [],
                }
            entries[group]["entries"].append(entry)

    # Generate html
    with open("%s/%s.html" % (DST, SIMULATION), 'w') as html:
        html.write("""
          <html>
            <head>
            <style>
              table {border: 1px solid black; }
              td {padding: 0.4em 1em; text-align: right; }
              tr:nth-child(even) {background: #CCC}
              tr:nth-child(odd) {background: #FFF}
              td.broken { background: #AA0000 }
              td.alert { background: #FF9900 }
              td.warning { background: #FFDD00 }
              td.fine {background: #00AA00 }
            </style>
            </head>
            <body>
              <h1>%s</h1>
        """ % SIMULATION)
        for group in entries.values():
            html.write("""
              <h2>%s</h2>
              <table>
                <tr>
                  <th>Fecha</th>
                  <th>Usuarios que llegan al final</th>
                  <th>Duración</th>
                  <th>t < 800 ms</th>
                  <th>800 ms < t < 1200 ms</th>
                  <th>t > 1200 ms</th>
                  <th>Job URL</th>
                  <th>Report URL</th>
                </tr>
            """ % group['title'])
            for entry in group['entries']:
                entry['success_percent'] = 100 * int(entry['success'])/int(entry['users'])
                if entry['success_percent'] == 0:
                    entry['status'] = 'broken'
                elif entry['success_percent'] < 50:
                    entry['status'] = 'alert'
                elif entry['success_percent'] < 90:
                    entry['status'] = 'warning'
                else:
                    entry['status'] = 'fine'
                html.write("""
                <tr>
                  <td>%(date)s</td>
                  <td class='%(status)s'>%(success)s (%(success_percent).1f%%)</td>
                  <td>%(duration)s seconds</td>
                  <td>%(group1)s</td>
                  <td>%(group2)s</td>
                  <td>%(group3)s</td>
                  <td><a href='%(jobUrl)s'>Job</a></td>
                  <td><a href='%(reportUrl)s'>Report</a></td>
                </tr>
                """ % entry)
            html.write("""
              </table>
            """)
        html.write("""
       </body>
     </html>
     """)
