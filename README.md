# Gatling helpers


## haproxy-log-summary.py

Procesa log de haproxy y genera tabla markdown con una fila por request.

Ejemplo de uso:

1. Hacer tail del log de haproxy filtrando por el site donde se va a hacer el escenario a mano y filtrar peticiones innecesarias:
```
DOMAIN=
tail -f /var/log/haproxy.log | grep $DOMAIN | grep -vE "\.(css|gif|js|jpg|jpeg|svg|png|ico).*"
```
2. Copiar las requests a nav.txt en la misma carpeta que este script
3. Ejecutar el script y pegar la salida en Gitlab


## gatling-parse-trace.py

Procesa una traza de Gatling y genera un fichero html por cada request realizada en una simulación.

Procesa `results/log` y genera `results/pageNN.html`.

Ejemplo de uso:

* Editar `conf/logback.xml` y descomentar el logger con level TRACE
* Ejecutar la simulación redirigiendo la salida a `results/log`. Ejemplo:
```
SIMULATION=
./run.sh -s $SIMULATION -nr 2>&1 > ./results/log
```
* Ejecutar el script

## reports

Ver readme específico.
